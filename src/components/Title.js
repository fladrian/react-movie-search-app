import React from 'react'

const Title = ({children}) => {
	return (
		<h1 className='title is-3'>
			{children}
		</h1>
	)
}
export default Title